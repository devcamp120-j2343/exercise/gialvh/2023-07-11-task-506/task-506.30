//khai báo thư viện express
const express = require('express');
const { drinkRouter } = require('./app/routes/drinkRouter');
const {voucherRouter} = require(`./app/routes/voucherRouter`);
const {userRouter} = require(`./app/routes/userRouter`);
const {orderRouter} = require(`./app/routes/orderRouter`);

//khai báo app
const app = express();

//khai báo cổng chạy
const port = 8000;

app.use(`/`,drinkRouter);
app.use(`/`,voucherRouter);
app.use(`/`,userRouter);
app.use(`/`,orderRouter);

//khởi động app
app.listen(port,()=>{
    console.log("App listen on port ", port);
})