//khai báo thư viện express
const express = require("express");

//tạo ra router
const orderRouter = express.Router();


//router Get All orders
orderRouter.get("/orders",(req,res,next)=>{
    console.log("Get All orders");
    console.log(req.method);
    res.json({
        message : "Get all orders"
    })
})

//router Get a orders
orderRouter.get("/orders/:orderId",(req,res,next)=>{
    let orderId = req.params.orderId;
    console.log("Get a drinks");
    console.log(req.method);
    next();
    res.json({
        message : `Get drink with id ${orderId}`
    })
})

//router Post a orders
orderRouter.post("/orders/:orderId",(req,res,next)=>{
    console.log("Create a new drink");
    console.log(req.method);
    next();
    res.json({
        message : `create a new order`
    })
})

//router Put a orders
orderRouter.put("/orders/:orderId",(req,res,next)=>{
    let orderId = req.params.orderId;
    console.log("Update a order");
    console.log(req.method);
    next();
    res.json({
        message : `update order with id ${orderId}`
    })
})

//router Delete a orders
orderRouter.delete("/orders/:orderId",(req,res,next)=>{
    let orderId = req.params.orderId;
    console.log("delete a drink");
    console.log(req.method);
    next();
    res.json({
        message : `delete order with id ${orderId}`
    })
})

module.exports(orderRouter);