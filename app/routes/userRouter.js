//khai báo thư viện
const express = require('express');

//tạo router
const userRouter = express.Router();

//router get all user
userRouter.get("/users",(req,res,next)=>{
    console.log("get all users");
    console.log(req.method);
    next();
    res.json({
        message : 'Get all users'
    })
})

//router get user by id
userRouter.get("/users/:userId",(req,res,next)=>{
    console.log("Get users by Id");
    console.log(req.method);

    let userId = req.params.userId;
    next();
    res.json({
        message : `get users by id ${userId}`
    })
})

//router post user
userRouter.post("/users/:userId",(req,res,next)=>{
    console.log("Create user!");
    console.log(req.method);
    let userId = req.params.userId;
    next();
    res.json({
        message : `create user with id : ${userId}`
    })
})

//router put user
userRouter.put("/users/:userId",(req,res,next)=>{
    console.log("Update user!");
    console.log(req.method);
    let userId = req.params.voucherId;
    next();
    res.json({
        message : `update user with id : ${userId}`
    })
})

//router delete user
userRouter.delete("/users/:userId",(req,res,next)=>{
    console.log("Delete user!");
    console.log(req.method);
    let userId = req.params.userId;
    next();
    res.json({
        message : `delete user with id : ${userId}`
    })
})

module.exports(userRouter);