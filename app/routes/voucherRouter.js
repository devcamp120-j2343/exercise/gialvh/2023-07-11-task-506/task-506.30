//khai báo thư viện
const express = require('express');

//tạo router
const voucherRouter = express.Router();

//router get all voucher
voucherRouter.get("/vouchers",(req,res,next)=>{
    console.log("get all voucher");
    console.log(req.method);
    next();
    res.json({
        message : 'Get all voucher'
    })
})

//router get voucher by id
voucherRouter.get("/vouchers/:voucherId",(req,res,next)=>{
    console.log("Get voucher by Id");
    console.log(req.method);

    let voucherId = req.params.voucherId;
    next();
    res.json({
        message : `get voucher by id ${voucherId}`
    })
})

//router post voucher
voucherRouter.post("/vouchers/:voucherId",(req,res,next)=>{
    console.log("Create voucher!");
    console.log(req.method);
    let voucherId = req.params.voucherId;
    next();
    res.json({
        message : `create voucher with id : ${voucherId}`
    })
})

//router put voucher
voucherRouter.put("/vouchers/:voucherId",(req,res,next)=>{
    console.log("Update voucher!");
    console.log(req.method);
    let voucherId = req.params.voucherId;
    next();
    res.json({
        message : `update voucher with id : ${voucherId}`
    })
})

//router delete voucher
voucherRouter.delete("/vouchers/:voucherId",(req,res,next)=>{
    console.log("Delete voucher!");
    console.log(req.method);
    let voucherId = req.params.voucherId;
    next();
    res.json({
        message : `delete voucher with id : ${voucherId}`
    })
})

module.exports(voucherRouter);