//khai báo thư viện express
const express = require("express");

//tạo ra router
const drinkRouter = express.Router();


//router Get All Drinks
drinkRouter.get("/drinks",(req,res,next)=>{
    console.log("Get All drinks");
    console.log(req.method);
    res.json({
        message : "Get all drinks"
    })
})

//router Get a Drinks
drinkRouter.get("/drinks/:drinkId",(req,res,next)=>{
    let drinkId = req.params.drinkId;
    console.log("Get a drinks");
    console.log(req.method);
    next();
    res.json({
        message : `Get drink with id ${drinkId}`
    })
})

//router Post a Drinks
drinkRouter.post("/drinks/:drinkId",(req,res,next)=>{
    console.log("Create a new drink");
    console.log(req.method);
    next();
    res.json({
        message : `create a new drink`
    })
})

//router Put a Drinks
drinkRouter.put("/drinks/:drinkId",(req,res,next)=>{
    let drinkId = req.params.drinkId;
    console.log("Update a drink");
    console.log(req.method);
    next();
    res.json({
        message : `update drink with id ${drinkId}`
    })
})

//router Delete a Drinks
drinkRouter.delete("/drinks/:drinkId",(req,res,next)=>{
    let drinkId = req.params.drinkId;
    console.log("delete a drink");
    console.log(req.method);
    next();
    res.json({
        message : `delete drink with id ${drinkId}`
    })
})

module.exports(drinkRouter);